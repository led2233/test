@extends('layouts.app')

@section('content')    
        <div class="col-md-4 offset-md-4 mt-5">
            <h1 class="my-3 text-center">Iniciar sesión</h1>
            <div class="card">
                <div class="card-body">
                    <form id="form-test">
                        <div class="form-group">
                            <label for="Email1">Email </label>
                            <input type="email" class="form-control" v-model="email" id="usuario" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label for="Password1">Contraseña</label>
                            <input type="password" class="form-control" id="password" v-model="password"
                                placeholder="Ingresa tu clave">
                        </div>

                        <button type="button" class="btn btn-primary" @click="startSesion()">Enviar</button>
                    </form>

                </div>
            </div>
        </div>
@endsection

