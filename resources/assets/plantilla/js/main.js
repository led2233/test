import Axios from "axios";

new Vue({
   el: '#test_login', 
   data: {
       email: '',
       password: ''
   },
   methods:{

    startSesion: function(){
        axios.post('/start-sesion', {
            email: this.email,
            password: this.password
        }).then(function (response){
            toastr.success('Has iniciado sesión','Datos correctos', 'Success');

        }).catch(function (error){
            console.log(error)
            let err = error.response.data.errors
          
            let mensaje = "Error no identificado";
            if (err.hasOwnProperty('email')) {
                mensaje = err.email[0];
            }else if (err.hasOwnProperty('password')){
                mensaje =  err.password[0];
            }else if(err.hasOwnProperty('login')){
                mensaje =  err.login[0];
            }
            toastr.error('Error', mensaje, 'error');
        });
        
    }
   }
});