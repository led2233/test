let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.styles([
   'resources/assets/plantilla/css/bootstrap.css',
   'resources/assets/plantilla/css/toastr.css',
],'public/css/app.css')
.scripts([
   'resources/assets/plantilla/js/main.js',
   'resources/assets/plantilla/js/toastr.js',
   ], 'public/js/plantilla.js')
   .js(['resources/assets/js/app.js'],'public/js/app.js');
 



