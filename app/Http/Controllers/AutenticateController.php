<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LoginFormRequest;
use Illuminate\Support\Facades\Auth;

class AutenticateController extends Controller
{
    public function startSesion(LoginFormRequest $request)
    { 

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            return response()->json('Has iniciado sesión');
        }else{
            return response()->json(['error' => ['login' => ['Los datos que ingresaste son incorrectos']]]);
        }
    }
}
