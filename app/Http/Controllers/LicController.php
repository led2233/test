<?php

namespace App\Http\Controllers;

use App\Lic;
use Illuminate\Http\Request;

class LicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Lic  $lic
     * @return \Illuminate\Http\Response
     */
    public function show(Lic $lic)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Lic  $lic
     * @return \Illuminate\Http\Response
     */
    public function edit(Lic $lic)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lic  $lic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lic $lic)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lic  $lic
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lic $lic)
    {
        //
    }
}
