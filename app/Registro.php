<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registro extends Model
{
    protected $fillable = [
        'nombre', 'paterno','materno','genero', 'edad', 'civil','email', 'password', 'nivel', 'nivelLic', 'nivelPos'];

}
