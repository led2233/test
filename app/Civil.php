<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Civil extends Model
{
    protected $fillable = ['estadi', 'descripcion', 'activo'];

}
