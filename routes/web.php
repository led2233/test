<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts/app');
});

Route::post('start-sesion', 'AutenticateController@startSesion');

Route::post('/persona/registrar', 'RegistroController@store');

Route::get('/genero/genero', 'GeneroController@selectGenero');
Route::get('/civil/civil', 'CivilController@selectCivil');
Route::get('/nivel/nivel', 'NivelController@selectNivel');
